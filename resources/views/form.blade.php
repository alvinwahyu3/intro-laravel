<!DOCTYPE html>
<html>
<head>
	<title>Form </title>
</head>
<body>
	<h1><b>Buat Account Baru</b></h1>
	<form action="/welcome" method="POST">
        @csrf
		<h2>Sign Up Form</h2>
		
		<label >First name:</label><br>
		<input type="text" id="namadepan" name="firstname"><br><br>
		<label >Last name:</label><br>
		<input type="text" id="namabelakang" name="lastname">
		
		<h3>Gender:</h3>
		<input type="radio" id="male" name="gender" value="male">
		<label for="male">Male</label><br>
		<input type="radio" id="female" name="gender" value="female">
		<label for="female">Female</label><br>
		<input type="radio" id="other" name="gender" value="other">
		<label for="other">Other</label>
		<br><br>

		<label>Nationality</label>
		<select name="kebangsaan" id="kebangsaan">
			<option value="Indonesia">Indonesia</option>
			<option value="China">China</option>
			<option value="Amerika">Amerika</option>
			<option value="Japan">Japan</option>
		</select>

		<h2>Language Spoken</h2>
		<input type="checkbox" id="bahasa1" name="bahasa1" value="Bahasa Indonesia">
		<label for="bahasa1"> Bahasa Indonesia</label><br>
		<input type="checkbox" id="bahasa2" name="bahasa2" value="English">
		<label for="bahasa2"> English</label><br>
		<input type="checkbox" id="bahasa3" name="bahasa3" value="Other">
		<label for="bahasa3"> Other</label><br><br>

		<label>Bio:</label> <br>
		<textarea id="textarea" name="textarea" rows="4" cols="50">
		</textarea>
		<br>
		<button type="submit">Sign Up</button>
	</form>
</body>
</html>