<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('form');
    }

    public function form(Request $request) {
        $first  = $request->firstname;
        $last   = $request->lastname;  
        return view('welcome',compact('first','last'));
        //dd($first,$last)
    }
}
